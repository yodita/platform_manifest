Introduction
---------------
My personal build for everyday. A simple AOSP project with little extras, keep the pure experience. You can build, you can use, you are free.. 

May the Force be with you!

Sync
---------------

```bash
repo init -u https://gitlab.com/yodita/platform_manifest.git -b ten
```
Then to sync up:
```bash
repo sync -f --force-sync --no-tags --no-clone-bundle
```

Building
---------------

```bash
. build/envsetup.sh
lunch yodita_devicename-userdebug
mka bacon
```

Credits
---------------

- LineageOS
- AEX
- Syberia
- ArrowOS
- AOSiP
- PixelExperience
- And to anyone who has crossed on my way and offered their help, thanks!
